﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiguilleManager : AbstractSingleton<AiguilleManager>
{
    private Vector3 worldPosition = Vector3.zero;

    private Vector3 lastPosition;

    private bool playMove = false;

    private bool playStop = false;

    private float time = 0;

    private float adjacent;

    private float opposé;

    private float z;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    private bool startedMoving = false;
    private bool onButton = false;
    public void OnButton()
    {
        startedMoving = false;
        onButton = true;
        AudioManager.Instance.StopArrow();
    }

    public void LeftButton()
    {
        onButton = false;
        startedMoving = false;
        AudioManager.Instance.MoveArrow();
    }
    private void OnDisable()
    {
        startedMoving = false;
        onButton = false;
    }
    // Update is called once per frame
    void Update()
    {
        if(!onButton){
            if(worldPosition.Equals(Vector3.zero))
                worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            lastPosition = worldPosition;
            worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (!startedMoving)
            {
                if (Mathf.Abs(transform.rotation.z) > float.Epsilon)
                {
                    AudioManager.Instance.MoveArrow();
                    playMove = true;
                    playStop = false;
                    startedMoving = true;
                }
            }
            //position du point de rotation de l'aiguille : x=0, y=-1.25
            if (worldPosition.y > -1.25)
            {
                adjacent = worldPosition.y + 1.25f;
                opposé = -worldPosition.x;
                z = (Mathf.Atan(opposé / adjacent)) * (180 / Mathf.PI);
                transform.eulerAngles = new Vector3(0, 0, z);
            }
            /*
            if(!startedMoving){
                if(Vector3.Distance(worldPosition, lastPosition) > float.Epsilon && (playMove == false))
                {
                    
                }
                
                if(Vector3.Distance(worldPosition, lastPosition) < 0.00000000000000000001 && (playStop == false))
                {
                    time += Time.deltaTime;
                }
                else
                {
                    time = 0;
                }
                if(time > 0.2 && (playStop == false))
                {
                    AudioManager.Instance.StopArrow();
                    playStop = true;
                    playMove = false;
                }
                
            }
*/
            //lastZ = transform.rotation.z;
        }
    }

    //private float lastZ = 0f;
    public void SetVertical()
    {
        transform.eulerAngles = new Vector3(0, 0, 0);
    }
}
