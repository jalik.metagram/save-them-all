﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbianceManager : AbstractSingleton<AmbianceManager>
{
    [SerializeField]
    AudioSource sourceClock;
    [SerializeField]
    AudioSource sourceDay_end;
    [SerializeField]
    AudioSource[] sourceAmbiances;
    bool isFirstDay = true;

    private void Awake()
    {
        base.Awake();
    }

    private void OnDestroy()
    {
        base.OnDestroy();
    }

    void Start()
    {
        PlayRandomAmbianceSounds();
    }

    void PlayRandomAmbianceSounds(){
        float rand0 = Random.value;
        float rand1 = Random.value;

        if(rand0 > 0.5f){
            StartCoroutine(FadeInFadeOut(sourceAmbiances[0], 4f, false, sourceAmbiances[0].volume));
        }
        if(rand1 > 0.5f){
            StartCoroutine(FadeInFadeOut(sourceAmbiances[1], 4f, false, sourceAmbiances[1].volume));
        }

        if(rand0 <= 0.5f && rand1 <= 0.5f){
            StartCoroutine(FadeInFadeOut(sourceAmbiances[0], 4f, false, sourceAmbiances[0].volume));
        }
        
        if(!isFirstDay)
            StartCoroutine(FadeInFadeOut(sourceClock, 2f, false, sourceClock.volume));
    }

    public void ChangeDayAudio(){
        if(isFirstDay){
            isFirstDay = false;
            return;
        }
        sourceClock.Pause();
        sourceDay_end.Play();
        StartCoroutine(WaitBellEnd());
    }

    IEnumerator WaitBellEnd(){
        StartCoroutine(FadeInFadeOut(sourceAmbiances[0], 2f, true, sourceAmbiances[0].volume));
        StartCoroutine(FadeInFadeOut(sourceAmbiances[1], 2f, true, sourceAmbiances[1].volume));
        yield return new WaitForSeconds(sourceDay_end.clip.length - 4);
        PlayRandomAmbianceSounds();
    }

    IEnumerator FadeInFadeOut(AudioSource toFade, float fadeTime, bool fadeOut, float uniqueVol = 1)
    {
        float time = 0f;
        float vol = 1f;

        if(uniqueVol !=1)
            vol = uniqueVol;

        if(fadeOut){
            toFade.volume = vol;
            while (toFade.volume > float.Epsilon)
            {
                time += Time.deltaTime;
                toFade.volume = Mathf.Lerp(vol, 0f, time / fadeTime);
                yield return null;
            }
            toFade.Pause();
        }else{  
            toFade.volume = 0f;
            toFade.Play();
            while (toFade.volume < vol - float.Epsilon)
            {
                time += Time.deltaTime;
                toFade.volume = Mathf.Lerp(0f, vol, time / fadeTime);
                yield return null;
            }
        }
    }
}
