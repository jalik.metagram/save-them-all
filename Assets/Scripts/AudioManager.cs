﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : AbstractSingleton<AudioManager>
{
    [SerializeField] float maxMusicLevel = 0.3f;
    [SerializeField] private AudioSource sourceBackground;
    [SerializeField] private AudioSource backgroundSpecialHeal;
    [SerializeField] private AudioSource backgroundSpecialNoHeal;
    [SerializeField] private AudioSource breath;
    [SerializeField] private AudioSource source2;
    [SerializeField] private AudioSource arrowLoop;
    [SerializeField] private AudioSource energy;

    [SerializeField] private AudioClip mouseOnDontHeal;
    [SerializeField] private AudioClip dontHealAction;
    [SerializeField] private AudioClip mouseOnHeal;
    [SerializeField] private AudioClip healAction;
    [SerializeField] private AudioClip breathSound;
    [SerializeField] private AudioClip arrowLoopSound;
    [SerializeField] private AudioClip arrowEndSound;
    [SerializeField] private AudioClip music;
    [SerializeField] private AudioClip looseEnergy;
    [SerializeField] private AudioClip loosePv;

    private void Awake()
    {
        base.Awake();
    }

    private void OnDestroy()
    {
        base.OnDestroy();
    }

    private bool isButtonActive;

    public void SetButtonActive()
    {
        isButtonActive = true;
      //  middle = (SoinButton.position + NoSoinButton.position) / 2;
        backgroundSpecialHeal.volume = 0f;
        backgroundSpecialHeal.loop = true;
        backgroundSpecialHeal.Play();
        backgroundSpecialNoHeal.volume = 0f;
        backgroundSpecialNoHeal.loop = true;
        backgroundSpecialNoHeal.Play();

    }

    //private Vector3 middle;
    public void SetButtonInactive()
    {
        isButtonActive = false;
        backgroundSpecialHeal.Stop();
        backgroundSpecialNoHeal.Stop();
    }
    private Vector3 soinButtonPos = new Vector3(6.7f, 3.9f);
    private Vector3 noSoinButtonPos = new Vector3(-6.7f, 3.9f);
    [SerializeField] private Transform SoinButton;
    [SerializeField] private Transform NoSoinButton;
    [SerializeField] private float radiusNoSound;
    [SerializeField] private float radiusFullSound;
    // Update is called once per frame
    void Update()
    {
        if (isButtonActive)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            soinButtonPos.z = mousePos.z;
            noSoinButtonPos.z = mousePos.z;
            float distSoin = Vector3.Distance(mousePos, soinButtonPos);
            float distNoSoin = Vector3.Distance(mousePos, noSoinButtonPos);
            if ( distSoin < distNoSoin)
            {
                backgroundSpecialNoHeal.volume = 0f;
                backgroundSpecialHeal.volume = Mathf.InverseLerp(radiusNoSound, radiusFullSound, distSoin);
            }
            else
            {
                backgroundSpecialHeal.volume = 0f;
                backgroundSpecialNoHeal.volume = Mathf.InverseLerp(radiusNoSound, radiusFullSound, distNoSoin);
            }
        }
        else
        {
            if(backgroundSpecialHeal.isPlaying)
                backgroundSpecialHeal.Stop();
            if(backgroundSpecialNoHeal.isPlaying)
                backgroundSpecialNoHeal.Stop();
        }
    }

    public void LoseEnergySound(){
        energy.PlayOneShot(looseEnergy, 1.5f);
        //StartCoroutine(waitForSouce2Routine(looseEnergy, 2));
    }
    public void LosePvSound(){
        energy.PlayOneShot(loosePv, 1.5f);
        //StartCoroutine(waitForSouce2Routine(loosePv, 2));
    }

    IEnumerator waitForSouce2Routine(AudioClip clip, float scale)
    {
        while (source2.isPlaying)
            yield return null;
        source2.PlayOneShot(clip, scale);
    }
    
    public void MoveArrow()
    {
        arrowLoop.clip = arrowLoopSound;
        arrowLoop.volume = 0.5f;
        arrowLoop.loop = true;
        arrowLoop.Play();
    }

    public void StopMoveArrow()
    {
        arrowLoop.volume = 0f;
    }

    public void StopArrow()
    {
        arrowLoop.clip = arrowEndSound;
        arrowLoop.volume = 0.5f;
        arrowLoop.loop = false;
        arrowLoop.PlayOneShot(arrowEndSound);
    }

    public void HoverOnDontHeal()
    {
        Debug.Log("DESTROY THIS");
    }

    public void StopHoverOnDontHeal()
    {
        Debug.Log("DESTROY THIS");
    }

    public void ClickOnDontHeal()
    {
        SetButtonInactive();
        source2.loop = false;
        source2.PlayOneShot(dontHealAction);
    }
  /*  
    public void HoverOnHeal()
    {
        Debug.Log("DESTROY THIS");
    }

    public void StopHoverOnHeal()
    {
        Debug.Log("DESTROY THIS");
    }
*/
    public void ClickOnHeal()
    {
        source2.loop = false;
        source2.PlayOneShot(healAction);
    }
    private Coroutine coroutineToStop = null;
    [SerializeField] private float timeToFade = 0.4f;
    private IEnumerator FadeInFadeOut(AudioSource toFadeIn, AudioSource toFadeOut, float maxVol = 0, float uniqueFadeTime = 0.4f)
    {
        float time = 0f;
        float vol = 1f;

        if(maxVol != 0){
            vol = maxVol;
        }
        if(uniqueFadeTime != 0.4f){
            timeToFade = uniqueFadeTime;
        }

        while (toFadeIn.volume < vol - float.Epsilon && toFadeOut.volume > float.Epsilon)
        {
            time += Time.deltaTime;
            toFadeIn.volume = Mathf.Lerp(0f, vol, time / timeToFade);
            toFadeOut.volume = Mathf.Lerp(vol, 0f, time / timeToFade);
            yield return null;
        }

        timeToFade = 0.4f;
        coroutineToStop = null;
    }
    
    public void MakeSoundDontHealCursor()
    {
        MakeSound(mouseOnDontHeal);
    }

    public void ChangeVolume()
    {
        sourceBackground.Stop();
        MakeSoundDontHealCursor();
    }

    /// <summary>
    /// Lance la lecture d'un son
    /// </summary>
    /// <param name="originalClip"></param>
    public void MakeSound(AudioClip originalClip)
    {
        StartCoroutine(PlaySound(originalClip));
    }

    private IEnumerator PlaySound(AudioClip clip)
    {
        source2.PlayOneShot(clip);
        yield return new WaitForSeconds(clip.length);
        MakeSoundDontHealCursor();
    }

    public void OutBoutton()
    {
        source2.Stop();
        sourceBackground.Play();
    }
}

