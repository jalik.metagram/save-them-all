﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardManager : AbstractSingleton<CardManager>
{
    [SerializeField] private float xmin; //à partir de quand on commence à rotate

    [SerializeField] private float xmax; //quand on arrête de rotate

    [SerializeField] private float rotmax; //de combien on rotate au max

    private Vector3 worldPosition;

    private float x;

    private float rot;

    private void Awake()
    {
        base.Awake();
    }

    private void OnDestroy()
    {
        base.OnDestroy();
    }

    // Update is called once per frame
    void Update()
    {
        worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (worldPosition.x > xmin)
        {
            x = Mathf.InverseLerp(xmin, xmax, worldPosition.x);
            rot = Mathf.Lerp(0, rotmax, x);
            transform.eulerAngles = new Vector3(0, rot, 0);
        }

        if (worldPosition.x < -xmin)
        {
            x = Mathf.InverseLerp(-xmax, -xmin, worldPosition.x);
            rot = Mathf.Lerp(-rotmax, 0, x);
            transform.eulerAngles = new Vector3(0, rot, 0);
        }
    }

    public void SetVertical()
    {
        transform.eulerAngles = new Vector3(0, 0, 0);
    }
}
