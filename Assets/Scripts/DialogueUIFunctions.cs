﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueUIFunctions : MonoBehaviour
{
    public Image panelDiag; 
    public void NextNode()
    {
        StartCoroutine(SmoothTransi());
    }

    public IEnumerator SmoothTransi()
    {
        float time = 0f;
        Color colorPanel = panelDiag.material.color;
        while (panelDiag.material.color.a > 0.01f)
        {
            time += Time.deltaTime;
            colorPanel.a = Mathf.LerpUnclamped(1f, 0f, time * 2);
            panelDiag.material.color = colorPanel;
            yield return null;
        }

        colorPanel.a = 0f;
        panelDiag.material.color = colorPanel;
        time = 0f;
        while (panelDiag.material.color.a < 1f - float.Epsilon)
        {
            time += Time.deltaTime;
            colorPanel.a = Mathf.LerpUnclamped(0f, 1f, time * 2);
            panelDiag.material.color = colorPanel;
            yield return null;
        }
        colorPanel.a = 1f;
        panelDiag.material.color = colorPanel;
    }

    
    
    public void NextCard()
    {
        StartCoroutine(ToNextCard());
    }

    IEnumerator ToNextCard()
    {
        float time = 0f;
        Color colorPanel = panelDiag.material.color;
        while (panelDiag.material.color.a > 0.01f)
        {
            time += Time.deltaTime;
            colorPanel.a = Mathf.LerpUnclamped(1f, 0f, time);
            panelDiag.material.color = colorPanel;
            yield return null;
        }
        colorPanel.a = 0f;
        panelDiag.material.color = colorPanel;
        GameManager.Instance.EndEvent();
        time = 0f;
        while (panelDiag.material.color.a < 1f - float.Epsilon)
        {
            time += Time.deltaTime;
            colorPanel.a = Mathf.LerpUnclamped(0f, 1f, time);
            panelDiag.material.color = colorPanel;
            yield return null;
        }
        colorPanel.a = 1f;
        panelDiag.material.color = colorPanel;
    }
}
