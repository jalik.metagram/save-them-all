﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.InteropServices;

public class EventsCounter
{
    public bool hasVisited = false;
    public bool hasDestin = false;
    public bool hasSoinDestin = false;
    public bool hasNoSoinDestin = false;
}
[System.Serializable]
public class SerializableAchievements
{
    public bool endingVillageois;
    public bool endingNoEnergyNoHeal;
    public bool endingNoEnergyHeal;
    public bool ending7days;
    public string[] nodeNames;
    public bool[] nodeHasVisited;
    public bool[] nodeHasDestin;
    public bool[] nodeHasSoinDestin;
    public bool[] nodeHasNoSoinDestin;

    public SerializableAchievements(EndingManager em)
    {
        ending7days = em.Ending7days;
        endingVillageois = em.EndingVillageois;
        endingNoEnergyHeal = em.EndingNoEnergyHeal;
        endingNoEnergyNoHeal = em.EndingNoEnergyNoHeal;
        int nbNodes = em.eventsCounters.Count;
        nodeNames = new string[nbNodes];
        nodeHasDestin = new bool[nbNodes];
        nodeHasSoinDestin = new bool[nbNodes];
        nodeHasNoSoinDestin = new bool[nbNodes];
        nodeHasVisited = new bool[nbNodes];
        int i = 0;
        foreach (KeyValuePair<string, EventsCounter> ev in em.eventsCounters)
        {
            nodeNames[i] = ev.Key;
            nodeHasDestin[i] = ev.Value.hasDestin;
            nodeHasVisited[i] = ev.Value.hasVisited;
            nodeHasSoinDestin[i] = ev.Value.hasSoinDestin;
            nodeHasNoSoinDestin[i] = ev.Value.hasNoSoinDestin;
            ++i;
        }
    }
}

public class EndingManager : AbstractSingleton<EndingManager>
{
    protected override void Awake()
    {
        base.Awake();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
    }

    [DllImport("__Internal")] private static extern void SyncFiles();
    [DllImport("__Internal")] private static extern void WindowAlert(string message);
    [HideInInspector] public Dictionary<string, EventsCounter> eventsCounters;
    private bool endingVillageois;
    private bool endingNoEnergyNoHeal;
    private bool endingNoEnergyHeal;
    private bool ending7days;

    public bool Ending7days
    {
        get => ending7days;
        set
        {
            ending7days = value;
                writeEndings();
        }
    }

    public bool EndingVillageois
    {
        get => endingVillageois;
        set
        { 
            endingVillageois = value;
            writeEndings();
        }
    }

    public bool EndingNoEnergyHeal
    {
        get => endingNoEnergyHeal;
        set
        { 
            endingNoEnergyHeal = value;
                writeEndings(); 
        }
    }

    public bool EndingNoEnergyNoHeal
    {
        get => endingNoEnergyNoHeal;
        set
        { 
            
                endingNoEnergyNoHeal = value;
                writeEndings();    
        }
    }

    public void writeEndings()
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            SyncFiles();
        }
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/arbitrium.save";
        FileStream stream = new FileStream(path, FileMode.Create);
        
        SerializableAchievements toWrite = new SerializableAchievements(this);
        formatter.Serialize(stream, toWrite);
        stream.Close();
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            SyncFiles();
        }
    }

    private void OnApplicationQuit()
    {
        writeEndings();
    }

    private void readEndings()
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            SyncFiles();
        }
        string path = Application.persistentDataPath + "/arbitrium.save";
        if (File.Exists(path))
        {
            SerializableAchievements allreadyUnlocked;
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            allreadyUnlocked = (SerializableAchievements) formatter.Deserialize(stream);
            stream.Close();
            this.ending7days = allreadyUnlocked.ending7days;
            this.endingVillageois = allreadyUnlocked.endingVillageois;
            this.endingNoEnergyHeal = allreadyUnlocked.endingNoEnergyHeal;
            this.endingNoEnergyNoHeal = allreadyUnlocked.endingNoEnergyNoHeal;
            for (int i = 0; i < allreadyUnlocked.nodeNames.Length; ++i)
            {
                this.eventsCounters[allreadyUnlocked.nodeNames[i]].hasDestin = allreadyUnlocked.nodeHasDestin[i];
                this.eventsCounters[allreadyUnlocked.nodeNames[i]].hasVisited = allreadyUnlocked.nodeHasVisited[i];
                this.eventsCounters[allreadyUnlocked.nodeNames[i]].hasNoSoinDestin = allreadyUnlocked.nodeHasNoSoinDestin[i];
                this.eventsCounters[allreadyUnlocked.nodeNames[i]].hasSoinDestin = allreadyUnlocked.nodeHasSoinDestin[i];
                if (this.eventsCounters[allreadyUnlocked.nodeNames[i]].hasVisited)
                    ++nbEventsFound;
                if (this.eventsCounters[allreadyUnlocked.nodeNames[i]].hasNoSoinDestin)
                    ++nbDestinsFoud;
                if (this.eventsCounters[allreadyUnlocked.nodeNames[i]].hasSoinDestin)
                    ++nbDestinsFoud;
            }
        }
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            SyncFiles();
        }
    }

    public Evennement[] allEvents;

    private int nbAllEvents = 0;
    public int nbEventsFound;
    private int nbDestins = 0;
    public int nbDestinsFoud;

    public int NbDestins => nbDestins;
    public int NbDestinsFoud => nbDestinsFoud;
    public int NbAllEvents => nbAllEvents;
    public int NbEventsFound => nbEventsFound;

    private readonly string[] nodesWithDestin =
    {
        "1",
        "2","3","4_2","5","6","7","8","9","10","11","12","13","14","15","16","17","18",
        "19","20","21","22","24","25_2","26","27","28","29","30","31","32","33","37","38_1"
    };
    // Start is called before the first frame update
    void Start()
    {
        eventsCounters = new Dictionary<string, EventsCounter>();
        foreach (Evennement evennement in allEvents)
        {
            ++nbAllEvents;
            eventsCounters.Add(evennement.StartNode, new EventsCounter());
            if (nodesWithDestin.Contains(evennement.StartNode))
            {
                eventsCounters[evennement.StartNode].hasDestin = true;
                nbDestins += 2;
            }
        }
        readEndings();
    }

}
