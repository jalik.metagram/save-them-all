﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndingUIShower : MonoBehaviour
{
    private EndingManager em;

    [SerializeField] private TMP_Text textEndings;
    [SerializeField] private TMP_Text textNumbers;
    [SerializeField] private TMP_Text description;
    // Start is called before the first frame update
    void Start()
    {
        em = EndingManager.Instance;
        int nbEndingFound = 0;
        textEndings.text = "";
        if (em.EndingVillageois)
        {
            textEndings.text += "Fin 1 obtenue : Vous avez été banni du village." + Environment.NewLine;
            ++nbEndingFound;
        }

        if (em.EndingNoEnergyHeal)
        {
            textEndings.text += "Fin 2 obtenue : Vous avez accompli votre mission jusqu'au bout." + Environment.NewLine;
            ++nbEndingFound;
        }

        if (em.EndingNoEnergyNoHeal)
        {
            textEndings.text += "Fin 3 obtenue : Vous avez abandonné votre pouvoir." + Environment.NewLine;
            ++nbEndingFound;
        }

        if (em.Ending7days)
        {
            textEndings.text += "Fin 4 obtenue : Vous partez à la recherche d’un successeur." + Environment.NewLine;
            ++nbEndingFound;
        }

        textNumbers.text = "";
        textNumbers.text += " "+em.nbEventsFound.ToString() + " / " + em.NbAllEvents.ToString() + Environment.NewLine;
        textNumbers.text += " "+em.NbDestinsFoud.ToString() + " / " + em.NbDestins.ToString() + Environment.NewLine;
        textNumbers.text += " "+nbEndingFound.ToString() + " / 4";
        if (nbEndingFound == 0 && em.NbEventsFound == 0 && em.NbDestinsFoud == 0)
        {
            textEndings.gameObject.SetActive(false);
            textNumbers.gameObject.SetActive(false);
            description.gameObject.SetActive(false);
        }
    }

}
