﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergieManager : AbstractSingleton<EnergieManager>
{
    public int Current
    {
        get { return energieCurrent; }
    }
    public int Max
    {
        get { return energieMax; }
    }
    [SerializeField] private Slider energieBar;

    //[SerializeField] private Text energiePoints;

    [SerializeField] private int energieMax = 3;

    [SerializeField] private int energieCurrent = 3;

    [SerializeField] private PopUpManager defeatPopUp;

    [SerializeField]
    private GameObject CorneMaxG, CorneMaxG1, CorneMaxG2, CorneMaxG3, CorneMaxD, CorneMaxD1, CorneMaxD2, CorneMaxD3, CorneMax1G, CorneMax1G1, CorneMax1G2,
        CorneMax1D, CorneMax1D1, CorneMax1D2, CorneMax2G, CorneMax2G1, CorneMax2D, CorneMax2D1;

    [SerializeField]
    AudioManager audioManager;

    private bool alive = true;

    // Start is called before the first frame update
    void Start()
    {
        //energiePoints.text = energieCurrent.ToString();
        energieBar.value = (float)energieCurrent / energieMax;
        alive = (energieCurrent > 0);
    }

    public void loseEnergieMax()
    {
        energieMax -= 1;
        if (energieCurrent > energieMax)
            energieCurrent = energieMax;
    }
    
    public void loseEnergie(int value)
    {
        energieCurrent = Mathf.Clamp(energieCurrent - value, 0, energieMax);
        //energiePoints.text = energieCurrent.ToString();
        energieBar.value = (float)energieCurrent / energieMax;
        if(energieCurrent <= 0)
        {
            //energiePoints.text = "you're dead";
            alive = false;
//            defeatPopUp.popUp();
            gameObject.SetActive(false);
        }

        changeSprite();
        audioManager.LoseEnergySound();
    }

    public void restoreEnergie()
    {
        energieCurrent = energieMax;
        //energiePoints.text = energieCurrent.ToString();
        energieBar.value = (float)energieCurrent / energieMax;
        changeSprite();
    }

    public void changeSprite()
    {
        if(energieMax == 3 && energieCurrent == 3)
        {
            CorneMaxG.SetActive(true);
            CorneMaxD.SetActive(true);
            CorneMaxG1.SetActive(false);
            CorneMaxD1.SetActive(false);
            CorneMaxG2.SetActive(false);
            CorneMaxD2.SetActive(false);
            CorneMaxG3.SetActive(false);
            CorneMaxD3.SetActive(false);
            CorneMax1G.SetActive(false);
            CorneMax1D.SetActive(false);
            CorneMax1G1.SetActive(false);
            CorneMax1D1.SetActive(false);
            CorneMax1G2.SetActive(false);
            CorneMax1D2.SetActive(false);
            CorneMax2G.SetActive(false);
            CorneMax2D.SetActive(false);
            CorneMax2G1.SetActive(false);
            CorneMax2D1.SetActive(false);
        }

        if(energieMax == 3 && energieCurrent == 2)
        {
            CorneMaxG.SetActive(false);
            CorneMaxD.SetActive(false);
            CorneMaxG1.SetActive(true);
            CorneMaxD1.SetActive(true);
            CorneMaxG2.SetActive(false);
            CorneMaxD2.SetActive(false);
            CorneMaxG3.SetActive(false);
            CorneMaxD3.SetActive(false);
            CorneMax1G.SetActive(false);
            CorneMax1D.SetActive(false);
            CorneMax1G1.SetActive(false);
            CorneMax1D1.SetActive(false);
            CorneMax1G2.SetActive(false);
            CorneMax1D2.SetActive(false);
            CorneMax2G.SetActive(false);
            CorneMax2D.SetActive(false);
            CorneMax2G1.SetActive(false);
            CorneMax2D1.SetActive(false);
        }

        if (energieMax == 3 && energieCurrent == 1)
        {
            CorneMaxG.SetActive(false);
            CorneMaxD.SetActive(false);
            CorneMaxG1.SetActive(false);
            CorneMaxD1.SetActive(false);
            CorneMaxG2.SetActive(true);
            CorneMaxD2.SetActive(true);
            CorneMaxG3.SetActive(false);
            CorneMaxD3.SetActive(false);
            CorneMax1G.SetActive(false);
            CorneMax1D.SetActive(false);
            CorneMax1G1.SetActive(false);
            CorneMax1D1.SetActive(false);
            CorneMax1G2.SetActive(false);
            CorneMax1D2.SetActive(false);
            CorneMax2G.SetActive(false);
            CorneMax2D.SetActive(false);
            CorneMax2G1.SetActive(false);
            CorneMax2D1.SetActive(false);
        }

        if (energieMax == 3 && energieCurrent == 0)
        {
            CorneMaxG.SetActive(false);
            CorneMaxD.SetActive(false);
            CorneMaxG1.SetActive(false);
            CorneMaxD1.SetActive(false);
            CorneMaxG2.SetActive(false);
            CorneMaxD2.SetActive(false);
            CorneMaxG3.SetActive(true);
            CorneMaxD3.SetActive(true);
            CorneMax1G.SetActive(false);
            CorneMax1D.SetActive(false);
            CorneMax1G1.SetActive(false);
            CorneMax1D1.SetActive(false);
            CorneMax1G2.SetActive(false);
            CorneMax1D2.SetActive(false);
            CorneMax2G.SetActive(false);
            CorneMax2D.SetActive(false);
            CorneMax2G1.SetActive(false);
            CorneMax2D1.SetActive(false);
        }

        if (energieMax == 2 && energieCurrent == 2)
        {
            CorneMaxG.SetActive(false);
            CorneMaxD.SetActive(false);
            CorneMaxG1.SetActive(false);
            CorneMaxD1.SetActive(false);
            CorneMaxG2.SetActive(false);
            CorneMaxD2.SetActive(false);
            CorneMaxG3.SetActive(false);
            CorneMaxD3.SetActive(false);
            CorneMax1G.SetActive(true);
            CorneMax1D.SetActive(true);
            CorneMax1G1.SetActive(false);
            CorneMax1D1.SetActive(false);
            CorneMax1G2.SetActive(false);
            CorneMax1D2.SetActive(false);
            CorneMax2G.SetActive(false);
            CorneMax2D.SetActive(false);
            CorneMax2G1.SetActive(false);
            CorneMax2D1.SetActive(false);
        }

        if (energieMax == 2 && energieCurrent == 1)
        {
            CorneMaxG.SetActive(false);
            CorneMaxD.SetActive(false);
            CorneMaxG1.SetActive(false);
            CorneMaxD1.SetActive(false);
            CorneMaxG2.SetActive(false);
            CorneMaxD2.SetActive(false);
            CorneMaxG3.SetActive(false);
            CorneMaxD3.SetActive(false);
            CorneMax1G.SetActive(false);
            CorneMax1D.SetActive(false);
            CorneMax1G1.SetActive(true);
            CorneMax1D1.SetActive(true);
            CorneMax1G2.SetActive(false);
            CorneMax1D2.SetActive(false);
            CorneMax2G.SetActive(false);
            CorneMax2D.SetActive(false);
            CorneMax2G1.SetActive(false);
            CorneMax2D1.SetActive(false);
        }

        if (energieMax == 2 && energieCurrent == 0)
        {
            CorneMaxG.SetActive(false);
            CorneMaxD.SetActive(false);
            CorneMaxG1.SetActive(false);
            CorneMaxD1.SetActive(false);
            CorneMaxG2.SetActive(false);
            CorneMaxD2.SetActive(false);
            CorneMaxG3.SetActive(false);
            CorneMaxD3.SetActive(false);
            CorneMax1G.SetActive(false);
            CorneMax1D.SetActive(false);
            CorneMax1G1.SetActive(false);
            CorneMax1D1.SetActive(false);
            CorneMax1G2.SetActive(true);
            CorneMax1D2.SetActive(true);
            CorneMax2G.SetActive(false);
            CorneMax2D.SetActive(false);
            CorneMax2G1.SetActive(false);
            CorneMax2D1.SetActive(false);
        }

        if (energieMax == 1 && energieCurrent == 1)
        {
            CorneMaxG.SetActive(false);
            CorneMaxD.SetActive(false);
            CorneMaxG1.SetActive(false);
            CorneMaxD1.SetActive(false);
            CorneMaxG2.SetActive(false);
            CorneMaxD2.SetActive(false);
            CorneMaxG3.SetActive(false);
            CorneMaxD3.SetActive(false);
            CorneMax1G.SetActive(false);
            CorneMax1D.SetActive(false);
            CorneMax1G1.SetActive(false);
            CorneMax1D1.SetActive(false);
            CorneMax1G2.SetActive(false);
            CorneMax1D2.SetActive(false);
            CorneMax2G.SetActive(true);
            CorneMax2D.SetActive(true);
            CorneMax2G1.SetActive(false);
            CorneMax2D1.SetActive(false);
        }

        if (energieMax == 1 && energieCurrent == 0)
        {
            CorneMaxG.SetActive(false);
            CorneMaxD.SetActive(false);
            CorneMaxG1.SetActive(false);
            CorneMaxD1.SetActive(false);
            CorneMaxG2.SetActive(false);
            CorneMaxD2.SetActive(false);
            CorneMaxG3.SetActive(false);
            CorneMaxD3.SetActive(false);
            CorneMax1G.SetActive(false);
            CorneMax1D.SetActive(false);
            CorneMax1G1.SetActive(false);
            CorneMax1D1.SetActive(false);
            CorneMax1G2.SetActive(false);
            CorneMax1D2.SetActive(false);
            CorneMax2G.SetActive(false);
            CorneMax2D.SetActive(false);
            CorneMax2G1.SetActive(true);
            CorneMax2D1.SetActive(true);
        }
    }
}
