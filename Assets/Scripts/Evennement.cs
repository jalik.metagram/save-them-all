﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn;

[System.Serializable]
public class NodeItem
{
    public string Node;
    public Sprite Illustration;
}

[CreateAssetMenu(menuName = "Evennement")]
public class Evennement : ScriptableObject
{
    [SerializeField] private YarnProgram dialogue = null;
   // [SerializeField] private Sprite image;
    [SerializeField] private int nbLost;
    [SerializeField] private string startNode;
    [SerializeField] private List<NodeItem> illustartions;
    private bool initialized = false;
    

    public Sprite GetSprite(string nodeName = "")
    {
        if (nodeName.Equals(""))
            return illustartions[0].Illustration;
        else
        {
            foreach (NodeItem nodeItem in illustartions)
            {
                if (nodeItem.Node.Equals(nodeName))
                    return nodeItem.Illustration;
            }

            return null;
        }
            
    }
    public YarnProgram Dialogue { get { return dialogue; } }
   // public Sprite Image { get { return image; } }
    public string StartNode { get { return startNode; }}
    public int Nb_Life_Lost { get { return nbLost; } }
    
    
}
