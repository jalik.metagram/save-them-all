﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Yarn.Unity;

public static class ListExtender
{
    public static void Shuffle<T>(this IList<T> list)  
    {  
        int n = list.Count;  
        while (n > 1) {  
            n--;
            int k = UnityEngine.Random.Range(0, n + 1);  
            T value = list[k];  
            list[k] = list[n];  
            list[n] = value;  
        }  
    }
}

public class GameManager : AbstractSingleton<GameManager>
{
    protected override void Awake()
    {
        base.Awake();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
    }

    private List<List<Evennement>> days;
    [SerializeField] private List<Evennement> PoolRandom;
    [SerializeField] private List<Evennement> PoolJour1;
    [SerializeField] private List<Evennement> PoolJour2;
    [SerializeField] private List<Evennement> PoolJour3;
    [SerializeField] private List<Evennement> PoolJour4;
    [SerializeField] private List<Evennement> PoolJour5;
    [SerializeField] private List<Evennement> PoolJour6;
    [SerializeField] private List<Evennement> PoolJour7;
    [SerializeField] private int nbEventPerDay = 4;

    [SerializeField] private Evennement villageois1;
    [SerializeField] private YarnProgram[] endings = new YarnProgram[4];

    [SerializeField] private YarnProgram firstDeplete;
    [SerializeField] private string nodeFirstDeplete;
    [SerializeField] private YarnProgram restDeplete1;
    [SerializeField] private string nodeRestDeplete1;
    [SerializeField] private YarnProgram restDeplete2;
    [SerializeField] private string nodeRestDeplete2;
    [SerializeField] private Evennement[] specEvent;
    private int hasDeplete = 0;
    private bool hasVillageois = false;
    [SerializeField] private Evennement newDay;
    [SerializeField] private bool DEBUG = false;

    [SerializeField] private AiguilleManager aiguille;

    private bool noSoinOfDay = true;
    //0 : pas de soin alors qu'un jour
    //1 : soingne une dernière fois
    //2 : villagoies
    //3 : 7 jours
    public void end(int endNumber)
    {
        gameObject.SetActive(true);
        dr.gameObject.SetActive(true);
        logs.optionsEnd();
        ending = true;
        dr.startNode = "Ending_" + endNumber.ToString();
        refIllus.sprite = defaultSprite;
        if (EndingManager.Instance != null)
        {
            if (endNumber == 0)
                EndingManager.Instance.EndingNoEnergyNoHeal = true;
            else if(endNumber == 1)
                EndingManager.Instance.EndingNoEnergyHeal = true;
            else if(endNumber == 2)
                EndingManager.Instance.EndingVillageois = true;
            else
                EndingManager.Instance.Ending7days = true;
        }
        dr.StartDialogue();
    }
    // Start is called before the first frame update
    private DialogueRunner dr;
    void Start()
    {
        days = new List<List<Evennement>>();
        days.Add(PoolJour1);
        days.Add(PoolJour2);
        days.Add(PoolJour3);
        days.Add(PoolJour4);
        days.Add(PoolJour5);
        days.Add(PoolJour6);
        days.Add(PoolJour7);
        dr = GetComponentInChildren<DialogueRunner>();
        foreach (List<Evennement> list in days)
        {
            foreach (Evennement evennement in list)
            {
                dr.Add(evennement.Dialogue);
            }
        }

        foreach (Evennement evennement in PoolRandom)
        {
            dr.Add(evennement.Dialogue);
        }

        foreach (YarnProgram yarnProgram in endings)
        {
            dr.Add(yarnProgram);
        }
		if(!DEBUG){
			foreach (Evennement evennement in specEvent)
			{
				dr.Add(evennement.Dialogue);
			}
		}
        dr.Add(villageois1.Dialogue);
        dr.Add(firstDeplete);
        dr.Add(restDeplete1);
        dr.Add(restDeplete2);
        dr.Add(newDay.Dialogue);
        dr.AddCommandHandler("Soin", Soin);
        dr.AddCommandHandler("NoSoin", NoSoin);
        dr.AddCommandHandler("Degats", Degats);
        StartDay();
        endingsPersons = new List<string>();
    }
    

    public void Degats(string[] param)
    {
        
        //Debug.Log("OUI");
        EnergieManager.Instance.loseEnergie(1);
        //Debug.Log(EnergieManager.Instance.Current);
        if(EnergieManager.Instance.Current < 1)
            EndDay();
    }
    
    private List<string> endingsPersons;
    private void Soin(string[] param)
    {
        noSoinOfDay = false;
        if(param != null)
            endingsPersons.Add(param[0].Replace('_',' '));
        
        EnergieManager.Instance.loseEnergie(1);
        Evennement current = days[0][0];
        //Gestion des events
        if (current.StartNode == "4")
        {
            if(days.Count > 1)
            {
                days[1].Add(specEvent[5]);
            }
        }else if (current.StartNode == "23")
        {
            if(days.Count > 1)
            {
                days[1].Add(specEvent[4]);
            }
        }else if (current.StartNode == "25")
        {
            if(days.Count > 1)
            {
                days[1].Add(specEvent[2]);
            }
        }
        if (EndingManager.Instance != null)
        {
            EventsCounter toOut;
            EndingManager.Instance.eventsCounters.TryGetValue(current.StartNode, out toOut);
            if (toOut != null)
            {
                if (EndingManager.Instance.eventsCounters[current.StartNode].hasDestin){
                    if(!EndingManager.Instance.eventsCounters[current.StartNode].hasSoinDestin){
                        EndingManager.Instance.eventsCounters[current.StartNode].hasSoinDestin = true;
                        ++EndingManager.Instance.nbDestinsFoud;
                        EndingManager.Instance.writeEndings();
                    }
                }
            }
        }
        if(EnergieManager.Instance.Current < 1)
            EndDay();
    }
    
    private void NoSoin(string[] param)
    {
        if(param != null)
            endingsPersons.Add(param[0].Replace('_',' '));
        Evennement current = days[0][0];
        //Gestion des events
        if (current.StartNode == "4")
        {
            if(days.Count > 1)
            {
                days[1].Add(specEvent[6]);
            }
        }else if (current.StartNode == "23")
        {
            if(days.Count > 1)
            {
                days[1].Add(specEvent[3]);
            }
        }else if (current.StartNode == "25")
        {
            if (days.Count > 1)
            {
                days[1].Add(specEvent[6]);
            }
        }
        if (EndingManager.Instance != null)
        {
            EventsCounter toOut;
            EndingManager.Instance.eventsCounters.TryGetValue(current.StartNode, out toOut);
            if (toOut != null)
            {
                if (EndingManager.Instance.eventsCounters[current.StartNode].hasDestin)
                {
                    if(!EndingManager.Instance.eventsCounters[current.StartNode].hasNoSoinDestin){
                        EndingManager.Instance.eventsCounters[current.StartNode].hasNoSoinDestin = true;
                        ++EndingManager.Instance.nbDestinsFoud;
                        EndingManager.Instance.writeEndings();
                    }
                }
                    
            }
        }
    }
    
    public void StartDay()
    {
        AmbianceManager.Instance.ChangeDayAudio();
        if (playCrackSound)
        {
            AudioManager.Instance.LosePvSound();
            playCrackSound = false;
        }
        specialDialogue = false;
        noSoinOfDay = true;
        EnergieManager.Instance.restoreEnergie();
        if(!DEBUG){
            while (days[0].Count < nbEventPerDay && PoolRandom.Count > 0)
            {
                Evennement Random = PoolRandom[UnityEngine.Random.Range(0, PoolRandom.Count)];
                days[0].Add(Random);
                PoolRandom.Remove(Random);
            }
            days[0].Shuffle();
            days[0].Add(newDay);
            Evennement last = days[0][nbEventPerDay];
            Evennement first = days[0][0];
            days[0][0] = last;
            days[0][nbEventPerDay] = first;
        }
        StartEvent();
    }

    [SerializeField] private Image refIllus;
    [SerializeField] private Sprite defaultSprite;
    [SerializeField] private TMP_Text texteRencontre;
    [SerializeField] private TMP_Text numberRencontres;
    void StartEvent()
    {
        aiguille.SetVertical();
        Evennement current = days[0][0];
        Sprite toPut = current.GetSprite();
        if (toPut == null)
            toPut = defaultSprite;
        refIllus.sprite = toPut;
        //CardManager.Instance.SetVertical();
        if (EndingManager.Instance != null)
        {
            EventsCounter toOut;
            EndingManager.Instance.eventsCounters.TryGetValue(current.StartNode, out toOut);
            if (toOut != null)
            {
                if (!EndingManager.Instance.eventsCounters[current.StartNode].hasVisited)
                {
                    ++EndingManager.Instance.nbEventsFound;
                    EndingManager.Instance.eventsCounters[current.StartNode].hasVisited = true;
                    EndingManager.Instance.writeEndings();
                }
            }
        }
        
        int nbRencontresRestantesIncludingThis = days[0].Count; 
        if (nbRencontresRestantesIncludingThis <= nbEventPerDay)
        {
            if (nbRencontresRestantesIncludingThis == nbEventPerDay)
            {
                texteRencontre.gameObject.SetActive(true);
                numberRencontres.gameObject.SetActive(true);
            }
                
            numberRencontres.text = (nbEventPerDay - nbRencontresRestantesIncludingThis + 1).ToString() + " / " + nbEventPerDay.ToString();
        }
        dr.startNode = current.StartNode;
        dr.StartDialogue();
    }

    [SerializeField] private GameObject buttonNextSentence;
    [SerializeField] private TMP_Text textEnd;
    private int nbDisplayed = 0;
    public void NextEndingSentence()
    {
        if (endingsPersons.Count > 0)
        {
            if (nbDisplayed > 14)
            {
                nbDisplayed = 0;
                textEnd.text = "";
            }
            ++nbDisplayed;
            textEnd.text += (nbDisplayed > 1 ? Environment.NewLine : "") + endingsPersons[0];
            endingsPersons.RemoveAt(0);
        }
        else
        {
            SceneManager.LoadScene("MENU_FINAL");
        }
    }

    public GameObject[] toHideOnEnd;
    
    public void EndEvent()
    {
        if (ending)
        {
            if (playCrackSound)
            {
                playCrackSound = false;
                AudioManager.Instance.LosePvSound();
            }
            buttonNextSentence.SetActive(true);
            textEnd.gameObject.SetActive(true);
            foreach (GameObject go in toHideOnEnd)
            {
                go.SetActive(false);
            }
            NextEndingSentence();
        }
        else if(specialDialogue)
            StartDay();
        else
        {
            days[0].RemoveAt(0);
            if(days[0].Count == 0)
                EndDay();
            else
                StartEvent();
        }
        
    }

    [SerializeField] private GameObject[] toDeactivateWeird;

    public void nanidefuck()
    {
        foreach (GameObject o in toDeactivateWeird)
        {
            o.SetActive(false);
        }
        AudioManager.Instance.StopMoveArrow();

    }
    [SerializeField] private Logs logs;
    private bool playCrackSound = false;
    private bool ending = false;
    private bool specialDialogue = false;
    public void EndDay()
    {
        texteRencontre.gameObject.SetActive(false);
        numberRencontres.gameObject.SetActive(false);
        nanidefuck();
        days.RemoveAt(0);
        if(DEBUG && days.Count == 3)
            end(3);
        if (days.Count == 0)
        {
            end(3);
        }
        else
        {
            if (EnergieManager.Instance.Current < 1)
            {
                if (EnergieManager.Instance.Max == 1)
                {
                    if (noSoinOfDay)
                    {
                        end(0);
                        return;
                    }
                    else
                    {
                        playCrackSound = true;
                        end(1);
                        return;
                    }
                }
                else if (hasDeplete == 0)
                {
                    specialDialogue = true;
                    dr.startNode = nodeFirstDeplete;
                    gameObject.SetActive(true);
                    dr.gameObject.SetActive(true);
                    logs.optionsEnd();
                    refIllus.sprite = defaultSprite;
                    dr.StartDialogue();
                    ++hasDeplete;
                }
                else
                {
                    playCrackSound = true;
                    specialDialogue = true;
                    refIllus.sprite = defaultSprite;
                    if (hasDeplete == 1)
                        dr.startNode = nodeRestDeplete1;
                    else{
                        dr.startNode = nodeRestDeplete2;
                    }
                    nanidefuck();
                    ++hasDeplete;
                    gameObject.SetActive(true);
                    dr.gameObject.SetActive(true);
                    logs.optionsEnd();
                    dr.StartDialogue();
                    EnergieManager.Instance.loseEnergieMax();
                    
                }
            }
            else if (noSoinOfDay)
            {
                if (!hasVillageois)
                {
                    specialDialogue = true;
                    refIllus.sprite = defaultSprite;
                    dr.startNode = villageois1.StartNode;
                    logs.optionsEnd();
                    gameObject.SetActive(true);
                    dr.gameObject.SetActive(true);
                    dr.StartDialogue();
                    hasVillageois = true;
                }
                else if(EnergieManager.Instance.Max == 1)
                {
                    end(0);
                }
                else
                {
                    end(2);
                }
            }
            else
            {
                StartDay();
            }
        }
    }
}
