﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Logs : MonoBehaviour
{
    [SerializeField] private TMP_Text logs;
    [SerializeField] private TMP_Text dialogue;
    private string toPutInLog;

    public void lineEnd()
    {
        toPutInLog = dialogue.text;
    }

    private static string BeetweenLines = System.Environment.NewLine + System.Environment.NewLine;
    
    public void lineStart()
    {
        logs.text += (logs.text.Equals("") ? "" : BeetweenLines) + toPutInLog;
        toPutInLog = "";
    }

    public void optionsStart()
    {
        lineStart();
    }

    public void optionsEnd()
    {
        logs.text = "";
        toPutInLog = "";
    }
}
