﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpManager : AbstractSingleton<PopUpManager>
{
    [SerializeField] private Text text;
    // Start is called before the first frame update

    private void Awake()
    {
        base.Awake();
    }

    private void OnDestroy()
    {
        base.OnDestroy();
    }
    void Start()
    {
        gameObject.SetActive(false);
    }

    public void popUp()
    {
        gameObject.SetActive(true);
        text.text = "you're dead";
    }
}
