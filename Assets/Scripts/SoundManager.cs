﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : AbstractSingleton<SoundManager>
{
    [SerializeField] private Button healButton;

    [SerializeField] private Button notHealButton;

    private float later;

    private float time = -1;

    private void Awake()
    {
        base.Awake();
    }

    private void OnDestroy()
    {
        base.OnDestroy();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(time < 0)
        {
            //AudioManager.Instance.MakeSound() faut faire la bonne fonction dans audio manager pour avoir ça
            later = Random.Range(4f, 6f);
            time = later;
        }
        time -= Time.deltaTime;
    }
}
